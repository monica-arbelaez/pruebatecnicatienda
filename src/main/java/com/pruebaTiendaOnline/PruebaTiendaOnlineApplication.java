package com.pruebaTiendaOnline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaTiendaOnlineApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaTiendaOnlineApplication.class, args);
	}

}
