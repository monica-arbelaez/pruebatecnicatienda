package com.pruebaTiendaOnline.service;

import java.util.Optional;

import com.pruebaTiendaOnline.entity.Cliente;



public interface ClienteService {
	public Iterable<Cliente> findAll();
	public Optional<Cliente> findById(Long id);
	public Cliente save(Cliente cliente);
	public void deleteById(long id);
	public Optional<Cliente> findCedula(String Cedula);

}
