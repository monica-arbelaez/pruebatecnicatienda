package com.pruebaTiendaOnline.service;

import java.util.Optional;

import com.pruebaTiendaOnline.entity.Factura;


public interface FacturaService{
	public Iterable<Factura> findAll();
	public Optional<Factura> findById(Long id);
	public Factura save(Factura factura);
	public void deleteById(long id);
	//public Optional<Factura> findCedula(String Cedula);

}

