package com.pruebaTiendaOnline.service;


import java.util.Optional;
import com.pruebaTiendaOnline.entity.Producto;


public interface  ProductoService{
	public Iterable<Producto> findAll();
	public Optional<Producto> findById(Long id);
	public Producto save(Producto producto);
	public void deleteById(long id);
	//public Optional<Producto> findCedula(String Cedula);

}
