# PruebaTecnicaTienda

Prueba técnica accenture Tienda Online con Spring Boot 

**instrucciones para realizar la persistencia en la base de datos.
Las tablas se crearán automaticamente con el código de la aplicación**



 Para realizar el POST se mapea http://localhost:8080/api/pedido

{
  "cedula": "11111",
  "cireccion":"Calle123#122-21",
  "idProductos":[1,2],
  "cantidadProductos":[2,3]
}

Para realizar el POST de un cliente se mapea http://localhost:8080/api/cliente

{
  "cedula": "111231",
  "cireccion":"Calle123#122"
}

 Para realizar el GET de Cliente se mapea http://localhost:8080/api/cliente/111231  y se consulta por el numero de cedula

 Para realizar el PUT de Cliente se mapea http://localhost:8080/api/cliente/1  y se modifica por el id el cliente

 Para realizar el DELETE de Cliente se mapea http://localhost:8080/api/cliente/1  y se elimina por el id el cliente


 Para realizar el POST de un producto se mapea http://localhost:8080/api/producto

{
  "nombre": "Table",
  "precio": 90000
}
 Para realizar el GET de producto se mapea http://localhost:8080/api/producto/1  y se consulta por el id del producto

 Para realizar el PUT de producto se mapea http://localhost:8080/api/producto/1  y se modifica por el id el producto
 
 Para realizar el DELETE de producto se mapea http://localhost:8080/api/producto/1  y se elimina por el id el producto

